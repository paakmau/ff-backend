package com.example.ffbackend.repo;

import com.example.ffbackend.entity.Stock;

import org.springframework.data.jpa.repository.JpaRepository;

public interface StockRepo extends JpaRepository<Stock, Integer> {
    Stock findByCode(String code);
}