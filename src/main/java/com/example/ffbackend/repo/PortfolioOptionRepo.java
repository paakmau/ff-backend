package com.example.ffbackend.repo;

import com.example.ffbackend.entity.PortfolioOption;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface PortfolioOptionRepo extends JpaRepository<PortfolioOption, Integer> {
    List<PortfolioOption> findByUserId (Integer userId);
}
