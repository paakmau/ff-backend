package com.example.ffbackend.repo;

import com.example.ffbackend.entity.User;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Transactional;

public interface UserRepo extends JpaRepository<User, Integer> {
    @Transactional
    void deleteByUsername (String username);
    User findByUsername (String username);
}