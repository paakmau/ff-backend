package com.example.ffbackend.bl;

import java.util.Map;
import java.util.Random;

import com.example.ffbackend.da.StockDaService;
import com.example.ffbackend.exception.MyRuntimeException;
import com.example.ffbackend.vo.ResponseEnums;
import com.example.ffbackend.vo.StockVo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import lombok.var;

@Service
public class StockService {
    @Autowired
    RpcStocksService rpcStocksService;

    @Autowired
    StockDaService daService;

    Map<String, StockVo> stockMap;

    /**
     * @param code
     * 格式 000001.SZ
     * @return
     */
    public String getStockNameByCode(String code) {
        var stock = daService.getStock(code);
        if (stock == null)
            throw new MyRuntimeException(ResponseEnums.NO_RECORD, "股票代码不正确");
        return stock.getName();
    }

    public Double getStockPriceByCode(String code) {
        Random rand = new Random();
        return (rand.nextInt(60) + 600) / 100.0;
    }
}
