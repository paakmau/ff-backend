package com.example.ffbackend.bl;

import java.util.*;

import com.example.ffbackend.da.NewsDaService;
import com.example.ffbackend.entity.StockInPortfolio;
import com.example.ffbackend.entity.User;
import com.example.ffbackend.repo.NewsRepo;
import com.example.ffbackend.vo.*;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import lombok.var;

@Service
public class MultifactorService {
    @Autowired
    PortfolioService portfolioService;

    @Autowired
    StockService stockService;

    @Autowired
    RpcStyleFactorService rpcStyleFactorService;

    @Autowired
    UserService userService;

    public List<StockTipVo> getStockTips(Integer userId) {

        var tips = new ArrayList<StockTipVo>();
        tips.add(new StockTipVo("603327", "福蓉科技"));
        tips.add(new StockTipVo("603322", "超讯通信"));
        tips.add(new StockTipVo("600116", "三峡水利"));

        return tips;
    }

    public MultifactorDetailsVo getMultifactorDetails(Integer userId) {

        var fund = userService.getUserFund(userId);
        MultifactorDetailsVo res = new MultifactorDetailsVo(fund, 0.5, null);
        var portfolio = portfolioService.getPortfolio(userId);
        List<MultifactorDatasetVo> multifactorDatasets = new ArrayList<MultifactorDatasetVo>(portfolio.size());
        List<String> factorNames = rpcStyleFactorService.getAllFactors();
        for (StockInPortfolioVo stockInPortfolio :
                portfolio) {
            List<Float> factorNum = rpcStyleFactorService.getBeta(stockInPortfolio.getCode());
            HashMap<String,Float> factors = new HashMap<String,Float>();
            for (int i = 0; i < 512; i++) {
                final int index = i;
                if (factorNum.get(i)>0.0000001){
                    factors.put(factorNames.get(index),factorNum.get(index));
                };
            }
            multifactorDatasets.add(new MultifactorDatasetVo(
                        stockInPortfolio.getName(),
                        stockInPortfolio.getCode(),
                        factors,
                        stockService.getStockPriceByCode(stockInPortfolio.getCode()),
                        stockInPortfolio.getNum(),
                        500
            ));

        }
        res.setDataSource(multifactorDatasets);

        var factors_ = new ArrayList<HashMap<String, Float>>() {
            private static final long serialVersionUID = 3126410076252489098L;

            {
                add(new HashMap<String,Float>() {
                    private static final long serialVersionUID = 3126410076252489098L;
                    {
                        put("生态保护和环境治理业", 0.2f);

                    }
                });
                add(new HashMap<String,Float>() {
                    private static final long serialVersionUID = 3126410076252489098L;

                    {
                        put("资本市场服务", 0.67f);
                    }
                });
                add(new HashMap<String,Float>() {
                    private static final long serialVersionUID = 3126410076252489098L;

                    {
                        put("电信、广播电视和卫星传输服务", 0.56f);
                    }
                });
                add(new HashMap<String,Float>() {
                    private static final long serialVersionUID = 3126410076252489098L;

                    {
                        put("边缘计算", 0.78f);
                        put("移动互联网", 0.21f);
                    }
                });
                add(new HashMap<String,Float>() {
                    private static final long serialVersionUID = 3126410076252489098L;

                    {
                        put("360概念", 0.36f);
                    }
                });
            }
        };

        MultifactorDetailsVo res_ = new MultifactorDetailsVo(fund, 0.5, null);
        Random rand = new Random();
        for (var stockInPortfolio : portfolio)
            multifactorDatasets.add(new MultifactorDatasetVo(
                    stockService.getStockNameByCode(stockInPortfolio.getCode()), stockInPortfolio.getCode(),
                    factors_.get(rand.nextInt(factors_.size())),
                    stockService.getStockPriceByCode(stockInPortfolio.getCode()), stockInPortfolio.getNum(), 500));
        res_.setDataSource(multifactorDatasets);


        if(factorNames.size()==0 ){
            return res_;
        }
        return res;
    }
}
