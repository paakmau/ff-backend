package com.example.ffbackend.da;

import com.example.ffbackend.entity.Stock;
import com.example.ffbackend.repo.StockRepo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class StockDaService {
    @Autowired
    StockRepo stockRepo;

    public Stock getStock(String code) {
        return stockRepo.findByCode(code);
    }
}