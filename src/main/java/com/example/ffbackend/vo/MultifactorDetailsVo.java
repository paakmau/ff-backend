package com.example.ffbackend.vo;

import java.util.List;
import java.util.Map;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class MultifactorDetailsVo {

    Double balance;
    Double threhold;
    List<MultifactorDatasetVo> dataSource;
}