package com.example.ffbackend.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class RpcOptionVo {
    String id;
    String mode;
    Double OPEN;
    Double HIGH;
    Double LOW;
    Double CLOSE;
    Double chg;
    Double VOLUME;
    Double AMT;
    Double VOLATILITYRATIO;
    Double US_IMPLIEDVOL;

    public OptionVo createVo(int have){
        return new OptionVo(id, mode, OPEN, HIGH, LOW, CLOSE, chg, have, VOLUME, VOLATILITYRATIO, US_IMPLIEDVOL);
    }
}