package com.example.ffbackend.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class StockVo {
    String stockName;
    String stockNum;
    String latestPrice;
    String marketValue;
    String accumulativeTotal;
    String todayTotal;
}