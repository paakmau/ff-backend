package com.example.ffbackend.vo;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class OptionVo {
    /*
    {"id":"10001853.SH","mode":"认购","OPEN":0.4784,"HIGH":0.4994,"LOW":0.4776,
    "CLOSE":0.4981,"chg":0.0552966102,"VOLUME":0.0966,
    "AMT":4720188.0,"VOLATILITYRATIO":70.3623910533,"US_IMPLIEDVOL":0.0}
     */
    String id;
    String mode;
    Double OPEN;
    Double HIGH;
    Double LOW;
    Double CLOSE;
    Double chg;
    int HAVE;
    Double VOLUME;
    Double VOLATILITYRATIO;
    Double US_IMPLIEDVOL;
}
