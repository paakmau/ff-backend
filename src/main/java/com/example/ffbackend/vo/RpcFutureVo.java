package com.example.ffbackend.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Id;
@Data
@AllArgsConstructor
@NoArgsConstructor
public class RpcFutureVo {
    String id;
    String name;
    Double OPEN;
    Double HIGH;
    Double LOW;
    Double CLOSE;
    Double SETTLE;
    Double VOLUME;
    Double AMT;
    Double OI;
    Double VOLRATIO;
    Double chg;
    int days_left;

    public FutureVo createVo(int have) {
        return new FutureVo(id, name, OPEN, HIGH, LOW, CLOSE, SETTLE, have, VOLUME, AMT, OI, VOLRATIO, chg, days_left);
    }
}
