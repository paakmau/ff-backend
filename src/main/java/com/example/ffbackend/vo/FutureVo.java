package com.example.ffbackend.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class FutureVo {
    String id;
    String name;
    Double OPEN;
    Double HIGH;
    Double LOW;
    Double CLOSE;
    Double SETTLE;
    int HAVE;
    Double VOLUME;
    Double AMT;
    Double OI;
    Double VOLRATIO;
    Double chg;
    int days_left;
}
