package com.example.ffbackend.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Map;


@Data
@AllArgsConstructor
@NoArgsConstructor
public class MultifactorDatasetVo {
    String name;
    String code;
    Map<String, Float> factor;
    Double price;
    Integer share;
    Integer tip;
}