package com.example.ffbackend.entity;

import lombok.Data;

import javax.persistence.*;

@Entity
@Data
@Table(name = "portfolioOption")
public class PortfolioOption {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Integer id;

    Integer userId;
    String optionCode;
    Integer have;

    public PortfolioOption(Integer userId, String optionCode, Integer have) {
        this.userId = userId;
        this.optionCode = optionCode;
        this.have = have;
    }
}