package com.example.ffbackend;

import com.example.ffbackend.bl.RpcOptionFuturesService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest
public class OptionTest {
    @Autowired
    RpcOptionFuturesService rpcOptionFuturesService;

    @Test
    public void testAllOption(){
        List<String> optionList=rpcOptionFuturesService.GetAllOptions();
        for (String option:optionList){
            System.out.println(option);
        }
    }

    @Test
    public void testDeltaInfo(){
        List<String> assetIdList = new ArrayList<>();
        List<Integer> assetAmountList = new ArrayList<>();
        assetIdList.add("000001.SZ");assetIdList.add("000002.SZ");
        assetAmountList.add(105);assetAmountList.add(150);
        float cash = 50000;
        String beginT = "2019-9-11";
        String endT = "2019-9-11";
        String info=rpcOptionFuturesService.GetDeltaInfo("123",assetIdList,assetAmountList,cash, beginT, endT);
    }

    @Test
    public void testGammaInfo(){
        List<String> assetIdList = new ArrayList<>();
        List<Integer> assetAmountList = new ArrayList<>();
        assetIdList.add("000001.SZ");assetIdList.add("000002.SZ");
        assetAmountList.add(105);assetAmountList.add(150);
        float cash = 50000;
        String beginT = "2019-9-11";
        String endT = "2019-9-11";
        String info=rpcOptionFuturesService.GetGammaInfo("123",assetIdList,assetAmountList,cash,"000001.SZ","000002.SZ",0.5f,0.5f, beginT, endT);
    }

//    @Test
//    public void test(){
//        List<String> assetIdList = new ArrayList<>();
//        List<Integer> assetAmountList = new ArrayList<>();
//        assetIdList.add("000001.SZ");assetIdList.add("000002.SZ");
//        assetAmountList.add(105);assetAmountList.add(150);
//        float cash = 50000;
//        String beginT = "2019-9-11";
//        String endT = "2019-9-11";
//        rpcOptionFuturesService.GetBetaInfo()
//    }

}
