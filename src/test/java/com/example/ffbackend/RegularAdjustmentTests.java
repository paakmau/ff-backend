package com.example.ffbackend;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;

import com.example.ffbackend.controller.RegularAdjustmentController;
import com.example.ffbackend.vo.RegularAdjustmentIndexVo;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import lombok.var;

@RunWith(SpringRunner.class)
@SpringBootTest
public class RegularAdjustmentTests {
    @Autowired
    RegularAdjustmentController ctrl;

    @Test
    public void testA() {
        var temp = ctrl.getRegularAdjustmentIndexs(2148);
    }

    @Test
    public void testB() {
        var listA = new ArrayList<RegularAdjustmentIndexVo>();
        listA.add(new RegularAdjustmentIndexVo("233", 2.333, 1.22, null));
        var temp = ctrl.putRegularAdjustmentIndexs(2121, listA);
    }
}
