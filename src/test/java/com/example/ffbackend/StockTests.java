package com.example.ffbackend;

import static org.junit.Assert.assertEquals;

import com.example.ffbackend.bl.StockService;
import com.example.ffbackend.controller.StockController;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import lombok.var;

@RunWith(SpringRunner.class)
@SpringBootTest
public class StockTests {
	@Autowired
	StockController ctrl;

	@Autowired
	StockService service;

	@Test
	public void testGetStockName() {
		assertEquals("万科A", service.getStockNameByCode("000002.SZ"));
	}

	@Test
	public void testGetStockName2() {
		assertEquals("深振业A", service.getStockNameByCode("000006.SZ"));
	}

	@Test
	public void testStockCurrent() {
		var temp = ctrl.getStockCurrent("000002.SZ");
	}

	@Test
	public void testStockNews() {
		var temp = ctrl.getNewsByStockCode("000001.SZ");
	}
}
